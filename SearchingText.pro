TEMPLATE = app

QT += qml quick

CONFIG += c++11

SOURCES += main.cpp \
    src/applicationview.cpp \
    src/listitem.cpp
	
HEADERS += \
    src/applicationview.h \
    src/listitem.h

RESOURCES += \
    resource/SearchingText.qrc

OTHER_FILES += \
        qml/main.qml \
        qml/ButtonBar.qml \
        qml/TextInputBar.qml \
        qml/ListItem.qml
