#ifndef APPLICATIONVIEW_H
#define APPLICATIONVIEW_H

#include <QObject>
#include <QUrl>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include "listitem.h"

class ApplicationView: public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString initialUrl READ initialUrl WRITE setInitialUrl NOTIFY initialUrlChanged)
    Q_PROPERTY(QString searchingText READ searchingText WRITE setSearchingText NOTIFY searchingTextChanged)
    Q_PROPERTY(int maxScanUrls READ maxScanUrls WRITE setMaxScanUrls NOTIFY maxScanUrlsChanged)
    Q_PROPERTY(int maxThreads READ maxThreads WRITE setMaxThreads NOTIFY maxThreadsChanged)
    Q_PROPERTY(QList<QObject *> urlList READ urlList NOTIFY urlListChanged)

public:
    ApplicationView(QObject *parent = nullptr);
    ~ApplicationView();

    Q_INVOKABLE void start();
    Q_INVOKABLE void stop();

    QString initialUrl() const;
    void setInitialUrl(const QString &url);

    QString searchingText() const;
    void setSearchingText(const QString &text);

    int maxScanUrls() const;
    void setMaxScanUrls(int value);

    int maxThreads() const;
    void setMaxThreads(int value);

    QList<QObject *> urlList() const;

signals:
    void initialUrlChanged();
    void searchingTextChanged();
    void maxScanUrlsChanged();
    void maxThreadsChanged();
    void urlListChanged();

private slots:
    void replyFinished();
    void networkErrors(QNetworkReply::NetworkError errorCode);

private:
    QString m_initialUrl;
    QString m_searchingText;
    int m_maxScanUrls;
    int m_maxThreads;
    QList<QObject *> m_urlList;

    QNetworkAccessManager m_networkAccessManager;
    QNetworkReply *m_networkReply;
    QList<QUrl> m_links;
    QTime m_totalTimer;
    QString m_status;

    void findAllLinks(const QByteArray &content);
    bool isHttpRedirect() const;
    void reportRedirect();
    void nextStep();
};

#endif // APPLICATIONVIEW_H
