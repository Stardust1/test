#include "networkmanager.h"

const QRegExp urlRegExp("(https?://)([\\w\\.-]+)\\.([\\w\\.]{2,6})([/\\w\\.-]*)*/?");
const QRegExp junkUrl("\\.jpg|\\.png|\\.js|\\.gzip|\\.ico|\\.pdf|\\.flv");

NetworkManager::NetworkManager(QObject *parent)
    : QObject(parent)
{
    qDebug(Q_FUNC_INFO);
}

NetworkManager::~NetworkManager()
{
    qDebug(Q_FUNC_INFO);
}

void NetworkManager::start(QUrl url, QString expression, int linksCount, int threads)
{
    qDebug(Q_FUNC_INFO);
    m_networkReply = nullptr;
    m_expression = expression;
    m_linksCount = linksCount;
    m_threadsCount = threads;
    m_error = "";
    m_links.append(url);
    m_totalTimer.start();
    nextStep();
}

void NetworkManager::nextStep()
{
    qDebug(Q_FUNC_INFO);
    if(m_links.isEmpty())
    {
        double elapsedTime = m_totalTimer.elapsed();
        qDebug() << "Searching time: " << elapsedTime / 1000 << " sec";
        qDebug("List is empty!");
        return;
    }
    else if(m_linksCount == 0)
    {
        double elapsedTime = m_totalTimer.elapsed();
        qDebug() << "Searching time: " << elapsedTime / 1000 << " sec";
        qDebug("That's all Folks!");
        return;
    }

    QUrl url = m_links.at(0);
    m_links.pop_front();
    m_linksHistory.append(url);

    if (!url.isValid())
    {
        m_error = "Invalid URL";
        qDebug() << m_error << ": " << url.toString();
    }
    else
    {
        QNetworkRequest request(url);
        request.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("text/html"));
        m_networkReply = m_networkAccessManager.get(request);
        connect(m_networkReply, SIGNAL(finished()), this, SLOT(replyFinished()));
        connect(m_networkReply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(networkErrors(QNetworkReply::NetworkError)));
        connect(m_networkReply, SIGNAL(sslErrors(QList<QSslError>)), this, SLOT(sslErrors(QList<QSslError>)));
        m_timer.restart();
    }
}


void NetworkManager::replyFinished()
{
    qDebug(Q_FUNC_INFO);
    QUrl url = m_networkReply->url();
    qDebug() << url.toString();

    double elapsedTime = m_timer.elapsed();
    qDebug() << "Elapsed time: " << elapsedTime / 1000 << " sec";

    if (m_networkReply->error() == QNetworkReply::NoError)
    {
        if(isHttpRedirect())
        {
            reportRedirect();
        }
        else
        {
            QByteArray content = m_networkReply->readAll();
            findAllLinks(content);
            qDebug("Number of occurrences on this page: %d", content.count(m_expression.toLocal8Bit()));
        }
    }

    m_networkReply->deleteLater();
    --m_linksCount;
    nextStep();

    qDebug() << "";
}

void NetworkManager::findAllLinks(const QByteArray &content)
{
    qDebug(Q_FUNC_INFO);
    QString str = content.data();
    int pos = 0;
    while((pos = urlRegExp.indexIn(str, pos)) != -1)
    {
        if (pos != -1)
        {
            QString foundUrl = urlRegExp.cap(0);
            pos += foundUrl.length();
            bool isJunkUrl = foundUrl.indexOf(junkUrl, 0) != -1 ? true : false;
            QUrl url(foundUrl);

            if(url.isValid() && !isJunkUrl && !m_links.contains(url) && !m_linksHistory.contains(url))
                m_links.append(url);
        }
    }
}

bool NetworkManager::isHttpRedirect() const
{
    qDebug(Q_FUNC_INFO);
    int statusCode = m_networkReply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    return statusCode == 301 || statusCode == 302 || statusCode == 303 || statusCode == 305 || statusCode == 307 || statusCode == 308;
}

void NetworkManager::reportRedirect()
{
    qDebug(Q_FUNC_INFO);
    int statusCode = m_networkReply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    QUrl requestUrl = m_networkReply->request().url();
    qDebug() << "Request: " << requestUrl.toDisplayString() << " was redirected with code: " << statusCode;

    QVariant target = m_networkReply->attribute(QNetworkRequest::RedirectionTargetAttribute);

    if (!target.isValid())
        return;

    QUrl redirectUrl = target.toUrl();

    if (redirectUrl.isRelative())
        redirectUrl = requestUrl.resolved(redirectUrl);

    m_links.push_front(redirectUrl);
}

void NetworkManager::networkErrors(QNetworkReply::NetworkError errorCode)
{
    qDebug(Q_FUNC_INFO);
    switch(errorCode)
    {
        // network layer errors
        case QNetworkReply::ConnectionRefusedError:
            m_error = "the server is not accepting requests";
            break;
        case QNetworkReply::RemoteHostClosedError:
           m_error = "the remote server closed the connection prematurely, before the entire reply was received and processed";
            break;
        case QNetworkReply::HostNotFoundError:
            m_error = "invalid hostname";
            break;
        case QNetworkReply::TimeoutError:
            m_error = "the connection to the remote server timed out";
            break;
        case QNetworkReply::OperationCanceledError:
            m_error = "the operation was canceled via calls to abort() or close() before it was finished.";
            break;
        case QNetworkReply::SslHandshakeFailedError:
            m_error = "the SSL/TLS handshake failed and the encrypted channel could not be established. The sslErrors() signal should have been emitted.";
            break;
        case QNetworkReply::TemporaryNetworkFailureError:
            m_error = "the connection was broken due to disconnection from the network, however the system has initiated roaming to another access point. The request should be resubmitted and will be processed as soon as the connection is re-established.";
            break;
        case QNetworkReply::NetworkSessionFailedError:
            m_error = "the connection was broken due to disconnection from the network or failure to start the network.";
            break;
        case QNetworkReply::BackgroundRequestNotAllowedError:
            m_error = "the background request is not currently allowed due to platform policy.";
            break;
        case QNetworkReply::UnknownNetworkError:
            m_error = "an unknown network-related error was detected";
            break;

        // proxy errors
        case QNetworkReply::ProxyConnectionRefusedError:
            m_error = "the proxy server is not accepting requests";
            break;
        case QNetworkReply::ProxyConnectionClosedError:
            m_error = "the proxy server closed the connection prematurely, before the entire reply was received and processed";
            break;
        case QNetworkReply::ProxyNotFoundError:
            m_error = "invalid proxy hostname";
            break;
        case QNetworkReply::ProxyTimeoutError:
            m_error = "the connection to the proxy timed out or the proxy did not reply in time to the request sent";
            break;
        case QNetworkReply::ProxyAuthenticationRequiredError:
            m_error = "the proxy requires authentication in order to honour the request but did not accept any credentials offered (if any)";
            break;
        case QNetworkReply::UnknownProxyError:
            m_error = "an unknown proxy-related error was detected";
            break;

        // content errors
        case QNetworkReply::ContentAccessDenied:
            m_error = "the access to the remote content was denied";
            break;
        case QNetworkReply::ContentOperationNotPermittedError:
            m_error = "the operation requested on the remote content is not permitted";
            break;
        case QNetworkReply::ContentNotFoundError:
            m_error = "the remote content was not found at the server";
            break;
        case QNetworkReply::AuthenticationRequiredError:
            m_error = "the remote server requires authentication to serve the content but the credentials provided were not accepted (if any)";
            break;
        case QNetworkReply::ContentReSendError:
            m_error = "the request needed to be sent again, but this failed for example because the upload data could not be read a second time.";
            break;
        case QNetworkReply::ContentConflictError:
            m_error = "the request could not be completed due to a conflict with the current state of the resource.";
            break;
        case QNetworkReply::ContentGoneError:
            m_error = "the requested resource is no longer available at the server.";
            break;
        case QNetworkReply::UnknownContentError:
            m_error = "an unknown error related to the remote content was detected";
            break;

        // protocol errors
        case QNetworkReply::ProtocolUnknownError:
            m_error = "the Network Access API cannot honor the request because the protocol is not known";
            break;
        case QNetworkReply::ProtocolInvalidOperationError:
            m_error = "the requested operation is invalid for this protocol";
            break;
        case QNetworkReply::ProtocolFailure:
            m_error = "parsing error, invalid or unexpected responses, etc.";
            break;

        // server side errors
        case QNetworkReply::InternalServerError:
            m_error = "the server encountered an unexpected condition which prevented it from fulfilling the request.";
            break;
        case QNetworkReply::OperationNotImplementedError:
            m_error = "the server does not support the functionality required to fulfill the request.";
            break;
        case QNetworkReply::ServiceUnavailableError:
            m_error = "the server is unable to handle the request at this time.";
            break;
        case QNetworkReply::UnknownServerError:
            m_error = "an unknown error related to the server response was detected";
            break;

        default:
            m_error = "Unknown Error.";
    }
    qDebug() << "Error: " << m_error;
}

void NetworkManager::sslErrors(QList<QSslError> errorsList)
{
    qDebug(Q_FUNC_INFO);
    foreach (QSslError sslError, errorsList)
    {
      m_error = sslError.errorString();
      qDebug() << m_error;
    }
}
