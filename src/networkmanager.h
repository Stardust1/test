#ifndef NETWORKMANAGER_H
#define NETWORKMANAGER_H

#include <QObject>
#include <QUrl>
#include <QDebug>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>

class NetworkManager: public QObject
{
    Q_OBJECT

public:
    explicit NetworkManager(QObject *parent = nullptr);
    ~NetworkManager();
    void start(QUrl url, QString expression, int linksCount, int threads);

private slots:
    void replyFinished();
    void networkErrors(QNetworkReply::NetworkError errorCode);
    void sslErrors(QList<QSslError> errorsList);

private:
    QNetworkAccessManager m_networkAccessManager;
    QNetworkReply *m_networkReply;
    QString m_expression;
    int m_linksCount;
    int m_threadsCount;
    QTime m_timer;
    QTime m_totalTimer;
    QList<QUrl> m_links;
    QList<QUrl> m_linksHistory;
    QString m_error;

    void findAllLinks(const QByteArray &content);
    bool isHttpRedirect() const;
    void reportRedirect();
    void nextStep();
};

#endif // NETWORKMANAGER_H
