#ifndef LISTITEM_H
#define LISTITEM_H
#include <QObject>

class ListItem: public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString url READ url WRITE setUrl NOTIFY urlChanged)
    Q_PROPERTY(QString status READ status WRITE setStatus NOTIFY statusChanged)
    Q_PROPERTY(int occurrences READ occurrences WRITE setOccurrences NOTIFY occurrencesChanged)

public:
    ListItem(QObject *parent = nullptr);
    ~ListItem();

    QString url() const;
    void setUrl(const QString &link);

    QString status() const;
    void setStatus(const QString &state);

    int occurrences() const;
    void setOccurrences(int value);

signals:
    void urlChanged();
    void statusChanged();
    void occurrencesChanged();

private:
    QString m_url;
    QString m_status;
    int m_occurrences;
};

#endif // LISTITEM_H
