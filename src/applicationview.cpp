#include "applicationview.h"
#include <QDebug>

const QRegExp urlRegExp("(https?://)([\\w\\.-]+)\\.([\\w\\.]{2,6})([/\\w\\.-]*)*/?");
const QRegExp junkUrl("\\.jpg|\\.png|\\.js|\\.gzip|\\.ico|\\.pdf|\\.flv");

ApplicationView::ApplicationView(QObject *parent)
    : QObject(parent)
{
    qDebug(Q_FUNC_INFO);

    setInitialUrl("");
    setSearchingText("");
    setMaxScanUrls(0);
    setMaxThreads(0);
}

ApplicationView::~ApplicationView()
{
    qDebug(Q_FUNC_INFO);
    qDeleteAll(m_urlList.begin(), m_urlList.end());
    m_urlList.clear();
}

void ApplicationView::start()
{
    qDebug(Q_FUNC_INFO);
    m_networkReply = nullptr;
    m_links.append(m_initialUrl);
    m_totalTimer.start();
    nextStep();
}

void ApplicationView::stop()
{
    qDebug(Q_FUNC_INFO);
}

QString ApplicationView::initialUrl() const
{
    return m_initialUrl;
}

void ApplicationView::setInitialUrl(const QString &url)
{
    if(url == m_initialUrl)
        return;

    m_initialUrl = url;
    emit initialUrlChanged();
}

QString ApplicationView::searchingText() const
{
    return m_searchingText;
}

void ApplicationView::setSearchingText(const QString &text)
{
    if(text == m_searchingText)
        return;

    m_searchingText = text;
    emit searchingTextChanged();
}

int ApplicationView::maxScanUrls() const
{
    return m_maxScanUrls;
}

void ApplicationView::setMaxScanUrls(int value)
{
    if(value == m_maxScanUrls)
        return;

    m_maxScanUrls = value;
    emit maxScanUrlsChanged();
}

int ApplicationView::maxThreads() const
{
    return m_maxThreads;
}

void ApplicationView::setMaxThreads(int value)
{
    if(value == m_maxThreads)
        return;

    m_maxThreads = value;
    emit maxThreadsChanged();
}

QList<QObject *> ApplicationView::urlList() const
{
    return m_urlList;
}

void ApplicationView::nextStep()
{
    if(m_links.isEmpty())
    {
        double elapsedTime = m_totalTimer.elapsed();
        qDebug() << "Searching time: " << elapsedTime / 1000 << " sec";
        qDebug("List is empty!");
        return;
    }
    else if(m_maxScanUrls == 0)
    {
        double elapsedTime = m_totalTimer.elapsed();
        qDebug() << "Searching time: " << elapsedTime / 1000 << " sec";
        qDebug("That's all Folks!");
        return;
    }

    QUrl url = m_links.at(0);
    m_links.pop_front();

    if (!url.isValid())
    {
        qDebug() << "Invalid URL: " << url.toString();
    }
    else
    {
        QNetworkRequest request(url);
        request.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("text/html"));
        m_networkReply = m_networkAccessManager.get(request);
        connect(m_networkReply, SIGNAL(finished()), this, SLOT(replyFinished()));
        connect(m_networkReply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(networkErrors(QNetworkReply::NetworkError)));
    }
}


void ApplicationView::replyFinished()
{
    if(isHttpRedirect())
    {
        m_networkReply->deleteLater();
        nextStep();
        return;
    }

    QUrl url = m_networkReply->url();
    int occurrences = 0;

    if (m_networkReply->error() == QNetworkReply::NoError)
    {
        QByteArray content = m_networkReply->readAll();
        findAllLinks(content);
        m_status = "success";
        occurrences = content.count(m_searchingText.toLocal8Bit());
    }

    auto item = new ListItem();
    item->setUrl(url.toString());
    item->setStatus(m_status);
    item->setOccurrences(occurrences);

    m_urlList.append(item);
    emit urlListChanged();

    m_networkReply->deleteLater();
    --m_maxScanUrls;
    nextStep();
}

void ApplicationView::findAllLinks(const QByteArray &content)
{
    QString str = content.data();
    int pos = 0;
    while((pos = urlRegExp.indexIn(str, pos)) != -1)
    {
        if (pos != -1)
        {
            QString foundUrl = urlRegExp.cap(0);
            pos += foundUrl.length();
            bool isJunkUrl = foundUrl.indexOf(junkUrl, 0) != -1 ? true : false;
            QUrl url(foundUrl);

            if(url.isValid() && !isJunkUrl && !m_links.contains(url))
                m_links.append(url);
        }
    }
}

bool ApplicationView::isHttpRedirect() const
{
    int statusCode = m_networkReply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    return statusCode == 301 || statusCode == 302 || statusCode == 303 || statusCode == 305 || statusCode == 307 || statusCode == 308;
}

void ApplicationView::networkErrors(QNetworkReply::NetworkError errorCode)
{
    qDebug(Q_FUNC_INFO);
    switch(errorCode)
    {
        // network layer errors
        case QNetworkReply::ConnectionRefusedError:
            m_status = "the server is not accepting requests";
            break;
        case QNetworkReply::RemoteHostClosedError:
           m_status = "the remote server closed the connection prematurely, before the entire reply was received and processed";
            break;
        case QNetworkReply::HostNotFoundError:
            m_status = "invalid hostname";
            break;
        case QNetworkReply::TimeoutError:
            m_status = "the connection to the remote server timed out";
            break;
        case QNetworkReply::OperationCanceledError:
            m_status = "the operation was canceled via calls to abort() or close() before it was finished.";
            break;
        case QNetworkReply::SslHandshakeFailedError:
            m_status = "the SSL/TLS handshake failed and the encrypted channel could not be established. The sslErrors() signal should have been emitted.";
            break;
        case QNetworkReply::TemporaryNetworkFailureError:
            m_status = "the connection was broken due to disconnection from the network, however the system has initiated roaming to another access point. The request should be resubmitted and will be processed as soon as the connection is re-established.";
            break;
        case QNetworkReply::NetworkSessionFailedError:
            m_status = "the connection was broken due to disconnection from the network or failure to start the network.";
            break;
        case QNetworkReply::BackgroundRequestNotAllowedError:
            m_status = "the background request is not currently allowed due to platform policy.";
            break;
        case QNetworkReply::UnknownNetworkError:
            m_status = "an unknown network-related error was detected";
            break;

        // proxy errors
        case QNetworkReply::ProxyConnectionRefusedError:
            m_status = "the proxy server is not accepting requests";
            break;
        case QNetworkReply::ProxyConnectionClosedError:
            m_status = "the proxy server closed the connection prematurely, before the entire reply was received and processed";
            break;
        case QNetworkReply::ProxyNotFoundError:
            m_status = "invalid proxy hostname";
            break;
        case QNetworkReply::ProxyTimeoutError:
            m_status = "the connection to the proxy timed out or the proxy did not reply in time to the request sent";
            break;
        case QNetworkReply::ProxyAuthenticationRequiredError:
            m_status = "the proxy requires authentication in order to honour the request but did not accept any credentials offered (if any)";
            break;
        case QNetworkReply::UnknownProxyError:
            m_status = "an unknown proxy-related error was detected";
            break;

        // content errors
        case QNetworkReply::ContentAccessDenied:
            m_status = "the access to the remote content was denied";
            break;
        case QNetworkReply::ContentOperationNotPermittedError:
            m_status = "the operation requested on the remote content is not permitted";
            break;
        case QNetworkReply::ContentNotFoundError:
            m_status = "the remote content was not found at the server";
            break;
        case QNetworkReply::AuthenticationRequiredError:
            m_status = "the remote server requires authentication to serve the content but the credentials provided were not accepted (if any)";
            break;
        case QNetworkReply::ContentReSendError:
            m_status = "the request needed to be sent again, but this failed for example because the upload data could not be read a second time.";
            break;
        case QNetworkReply::ContentConflictError:
            m_status = "the request could not be completed due to a conflict with the current state of the resource.";
            break;
        case QNetworkReply::ContentGoneError:
            m_status = "the requested resource is no longer available at the server.";
            break;
        case QNetworkReply::UnknownContentError:
            m_status = "an unknown error related to the remote content was detected";
            break;

        // protocol errors
        case QNetworkReply::ProtocolUnknownError:
            m_status = "the Network Access API cannot honor the request because the protocol is not known";
            break;
        case QNetworkReply::ProtocolInvalidOperationError:
            m_status = "the requested operation is invalid for this protocol";
            break;
        case QNetworkReply::ProtocolFailure:
            m_status = "parsing error, invalid or unexpected responses, etc.";
            break;

        // server side errors
        case QNetworkReply::InternalServerError:
            m_status = "the server encountered an unexpected condition which prevented it from fulfilling the request.";
            break;
        case QNetworkReply::OperationNotImplementedError:
            m_status = "the server does not support the functionality required to fulfill the request.";
            break;
        case QNetworkReply::ServiceUnavailableError:
            m_status = "the server is unable to handle the request at this time.";
            break;
        case QNetworkReply::UnknownServerError:
            m_status = "an unknown error related to the server response was detected";
            break;

        default:
            m_status = "Unknown Error.";
    }
}
