#include "listitem.h"
#include <QDebug>

ListItem::ListItem(QObject *parent)
    : QObject(parent)
{
    qDebug() << Q_FUNC_INFO;
}

ListItem::~ListItem()
{
    qDebug() << Q_FUNC_INFO;
}

QString ListItem::url() const
{
    return m_url;
}

void ListItem::setUrl(const QString &link)
{
    if(link == m_url)
        return;

    m_url = link;
    emit urlChanged();
}

QString ListItem::status() const
{
    return m_status;
}

void ListItem::setStatus(const QString &state)
{
    if(state == m_status)
        return;

    m_status = state;
    emit statusChanged();
}

int ListItem::occurrences() const
{
    return m_occurrences;
}

void ListItem::setOccurrences(int value)
{
    if(value == m_occurrences)
        return;

    m_occurrences = value;
    emit occurrencesChanged();
}
