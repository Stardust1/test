#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickItem>

#include "src/applicationview.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    qmlRegisterType<ApplicationView>("ApplicationView", 1, 0, "AppView");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));

    return app.exec();
}

