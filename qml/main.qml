import QtQuick 2.5
import QtQuick.Window 2.2
import QtGraphicalEffects 1.0
import ApplicationView 1.0

import "../qml"

Window {
    property string textColor: "#FFFFFF"

    visible: true
    width: 960
    height: 850
    minimumWidth: 960
    minimumHeight: 850
    maximumWidth: 960
    maximumHeight: 850
    title: "ololo"
    color: "#404244"

    AppView
    {
        id: appview
    }

    TextInputBar
    {
        id: textInputBar
    }

    ListView {
        id: linksList
        width: 900
        height: 600
        anchors {
            top: textInputBar.bottom
            topMargin: 30
            left: parent.left
            leftMargin: 30
        }

        model: appview.urlList

        header: ListItem {
            text1: "Url"
            text2: "Status"
            text3: "Occurrences"
        }

        delegate: ListItem {
            text1: model.modelData.url
            text2: model.modelData.status
            text3: model.modelData.occurrences
        }
    }

    ButtonBar {
        id: bottomButtonBar
    }
}

