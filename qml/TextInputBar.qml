import QtQuick 2.0
import QtQuick.Controls 1.4

Item {
    property alias url: inputUrl.text
    property alias text: inputText.text
    property alias links: maxUrls.text
    property alias threads: maxThreads.text

    width: 900
    height: 30
    anchors {
        left: parent.left
        leftMargin: 30
        top: parent.top
        topMargin: 30
    }

    Label {
        anchors.bottom: inputUrl.top
        anchors.left: inputUrl.left
        text: "Enter Url"
        color: textColor
        font.pixelSize: 14
    }

    TextField {
        id: inputUrl
        anchors.left: parent.left
        width: 300
        height: 30
        text: "http://doc.qt.io/qt-5/qtnetwork-downloadmanager-downloadmanager-cpp.html"
        placeholderText: "Enter Url"
    }

    Label {
        anchors.bottom: inputText.top
        anchors.left: inputText.left
        text: "Enter text"
        color: textColor
        font.pixelSize: 14
    }

    TextField {
        id: inputText
        anchors.left: inputUrl.right
        anchors.leftMargin: 20
        width: 300
        height: 30
        text: "<head>"
        placeholderText: "Enter Text"
    }

    Label {
        anchors.bottom: maxUrls.top
        anchors.left: maxUrls.left
        text: "Urls count"
        color: textColor
        font.pixelSize: 14
    }

    TextField {
        id: maxUrls
        anchors.left: inputText.right
        anchors.leftMargin: 20
        width: 100
        height: 30
        text: "1"
        validator: IntValidator {
            bottom: 1
            top: 2147483647
        }
    }

    Label {
        anchors.bottom: maxThreads.top
        anchors.left: maxThreads.left
        text: "Threads count"
        color: textColor
        font.pixelSize: 14
    }

    TextField {
        id: maxThreads
        anchors.left: maxUrls.right
        anchors.leftMargin: 20
        width: 100
        height: 30
        text: "1"
        validator: IntValidator {
            bottom: 1
            top: 2147483647
        }
    }
}

