import QtQuick 2.0

Item {
    width: 900
    height: 75

    anchors {
        top: linksList.bottom
        topMargin: 50
        left: parent.left
        leftMargin: 30
    }

    Rectangle {
        id: startButton
        width: 250
        height: 75
        radius: 5
        border.color: "#333333"

        anchors {
            left: parent.left
        }

        gradient: Gradient {
            GradientStop { position: 0.0; color: "#00DD00" }
            GradientStop { position: 1.0; color: "#003300" }
        }

        Text {
            anchors.centerIn: parent
            text: "Start"
            font.pixelSize: 25
            color: textColor
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                appview.initialUrl = textInputBar.url
                appview.searchingText = textInputBar.text
                appview.maxScanUrls = textInputBar.links
                appview.maxThreads = textInputBar.threads
                appview.start()
            }
        }
    }

    Rectangle {
        id: stopButton
        width: 250
        height: 75
        radius: 5
        border.color: "#333333"

        anchors {
            centerIn: parent
        }

        gradient: Gradient {
            GradientStop { position: 0.0; color: "#DD0000" }
            GradientStop { position: 1.0; color: "#330000" }
        }

        Text {
            anchors.centerIn: parent
            text: "Stop"
            font.pixelSize: 25
            color: textColor
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                appview.stop()
            }
        }
    }

    Rectangle {
        id: quitButton
        width: 250
        height: 75
        radius: 5
        border.color: "#333333"

        anchors {
            right: parent.right
        }

        gradient: Gradient {
            GradientStop { position: 0.0; color: "#555555" }
            GradientStop { position: 1.0; color: "#000000" }
        }

        Text {
            anchors.centerIn: parent
            text: "Quit"
            font.pixelSize: 25
            color: textColor
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                Qt.quit()
             }
        }
    }
}

