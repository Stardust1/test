import QtQuick 2.0

Item {
    property alias text1: column1.text
    property alias text2: column2.text
    property alias text3: column3.text

    width: 900
    height: 30

    Row {
        spacing: -1
        Rectangle {
            width: 600
            height: 30
            border.color: "#FFFFFF"
            border.width: 1
            color: "#404244"

            Text {
                id: column1
                text: ""
                anchors.centerIn: parent
                font.pixelSize: 14
                color: textColor
            }
        }

        Rectangle {
            width: 200
            height: 30
            border.color: "#FFFFFF"
            border.width: 1
            color: "#404244"

            Text {
                id: column2
                text: ""
                anchors.centerIn: parent
                font.pixelSize: 14
                color: textColor
            }
        }

        Rectangle {
            width: 100
            height: 30
            border.color: "#FFFFFF"
            border.width: 1
            color: "#404244"

            Text {
                id: column3
                text: ""
                anchors.centerIn: parent
                font.pixelSize: 14
                color: textColor
            }
        }
    }
}

